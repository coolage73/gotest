package main

import (
	"go.uber.org/zap"
	"gopkg.in/alecthomas/kingpin.v2"
	core "k8s.io/api/core/v1"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"

	"gitlab.com/coolage73/goTest/kubernetes"
)

var (
	kubecfg   = kingpin.Flag("kubeconfig", "Path to kubeconfig file. Leave unset to use in-cluster config.").String()
	apiserver = kingpin.Flag("master", "Address of Kubernetes API server. Leave unset to use in-cluster config.").String()

	nodeName = kingpin.Flag("nodename", "node name to change condition").Short('n').Required().String()
)

func contains(s []core.ConditionStatus, substr string) bool {
	for _, v := range s {
		if v == core.ConditionStatus(substr) {
			return true
		}
	}
	return false
}

func filter(conds []core.NodeCondition, f func(cond core.NodeCondition) bool) *[]core.NodeCondition {
	filtered := []core.NodeCondition{}

	for _, c := range conds {
		if f(c) {
			filtered = append(filtered, c)
		}
	}
	return &filtered
}

type Condition struct {
	Status             core.ConditionStatus
	Type               core.NodeConditionType
	LastTransitionTime meta.Time
}

func do(nodeName string, log *zap.Logger) {
	k := kubernetes.NewClient(log, apiserver, kubecfg)
	k.CordonNode(nodeName)
}

func main() {
	kingpin.Parse()
	log, err := zap.NewProduction()
	kingpin.FatalIfError(err, "cannot create log")

	do(*nodeName, log)
}
